package com.vadenent;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"", "/main"})
public class HeadsOrTails extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			  throws ServletException, IOException {
		
		// Session start sets nextPage to "InitialPage.jsp"
		// Normal playing of the game sets the nextPage to "MainPage.jsp"
		// Ending the game sets the nextPage to "EndingPage.jsp"
		// doPost sets nextPage based on the user response
		
		// Retrieve the nextPage
		HttpSession session = request.getSession();		
		String nextPage = (String) session.getAttribute("nextPage");
		
		request.getRequestDispatcher("/WEB-INF/InitialPage.jsp").forward(request, response);

	}	// end doGet
	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			  throws ServletException, IOException {

		HttpSession session = request.getSession();

		CoinFlipper cf = new CoinFlipper();	
		cf = (CoinFlipper) session.getAttribute("coinFlipper");
		
		String userRequest = cf.getUserRequest(request);			//value in request scope - not session scope	
		
		switch (userRequest) {

			// if the user makes a "Heads" or "Tails" guess, flip the coin and calculate the statistics,
			// and then save the results back to the session scope object.
			case  "Heads":
			case  "Tails":
				cf.flipTheCoin();							
				session.setAttribute("coinFlipper", cf);	 
				session.setAttribute("nextPage", "MainPage");
				break;
				
			// of the user has chosen "Reset" or to "Restart", then reset the session scope state
			// back to its initial values and point to the "InitialPage" to start the game over.
			case  "Reset":
			case  "Restart":
				cf.reset(session);		
				session.setAttribute("nextPage", "InitialPage");
				break;
				
			// if the user has chosen to end the game, the view no longer needs to show the result
			// of the last flip, but should still show the summary statistics.
			case  "EndGame":
				session.setAttribute("nextPage", "EndingPage");
				
			default:	
		}  // end switch

		response.sendRedirect("/Project4a");

	}	// end doPost

}
