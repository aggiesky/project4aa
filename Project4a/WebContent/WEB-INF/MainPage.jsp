<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="css/Basic.css" rel=stylesheet type="text/css" media="screen">
<title>Coin Flipper</title>

</head>
<body>
<div class="top-level">
<h1>Welcome to the <span style="font-style: italic;">Coin Flipper</span></h1>

	<div class="section">
		<c:if test='${nextPage eq "InitialPage.jsp"}'>
			<p>Select the "Heads" or "Tails" button to play the game.<p>
		</c:if>
		<c:if test='${nextPage eq "MainPage.jsp"}'>
			<h2>${coinFlipper.bannerForPreviousGame}</h2>
			<p style="font-weight: bolder;">The Guess was: ${coinFlipper.previousGuess}	&nbsp &nbsp &nbsp	The Result was: ${coinFlipper.previousFlipResult}</p>
		</c:if>
		<c:if test='${nextPage eq "EndingPage.jsp"}'>
				<h2>Thanks for playing!</h2>
		</c:if>
	</div>
	
	<div class="section" style="padding-bottom: 40px; margin-left: 10px;">
		<c:if test='${not (nextPage eq "EndingPage.jsp")}'>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Heads">
		        </form>
		    </div>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Tails">
		        </form>
		    </div>
	    </c:if>
	 	<c:if test='${nextPage eq "MainPage.jsp"}'>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="EndGame">
		        </form>
		    </div>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Reset">
		        </form>
		    </div>    
	    </c:if>
	</div>
	
	<hr style="width: 75%; float: left;">
	<br>
	
	<div class="section">	
		<h2>Game Statistics</h2>
		<p>Games Played:  ${coinFlipper.totalNumberOfFlips}  &nbsp&nbsp  Winning percentage: ${coinFlipper.totalWinningPercentage} %</p>
		<p>${coinFlipper.totalNumberOfHeads} flips were Heads.	&nbsp&nbsp	${coinFlipper.totalNumberOfTails} flips were Tails.</p>
	</div>
		 <c:if test='${nextPage eq "EndingPage.jsp"}'>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Restart">
		        </form>
		    </div>
	    </c:if>
	
</div>
</body>
</html>