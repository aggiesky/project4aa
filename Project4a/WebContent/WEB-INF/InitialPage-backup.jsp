<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="css/Basic.css" rel=stylesheet type="text/css" media="screen">
<title>Coin Flipper</title>

</head>
<body>
<div class="top-level">
<h1>Welcome to the <span style="font-style: italic;">Coin Flipper</span></h1>

	<div class="section">
		<p>Select the "Heads" or "Tails" button to play the game.<p>
	</div>

	<div class="section" style="padding-bottom: 40px; margin-left: 10px;">	
	    <div class="button"> 
	        <form method="POST" action="main" >
	            <input type="submit" name="userGuess" value="Heads">
	        </form>
	    </div>
	    <div class="button"> 
	        <form method="POST" action="main" >
	            <input type="submit" name="userGuess" value="Tails">
	        </form>
	    </div>
	</div>
	
	<hr style="width: 75%; float: left;">
	<br>
	
	<div class="section">	
		<h2>Game Statistics</h2>
		<p>Games Played:  ${coinFlipper.totalNumberOfFlips}</p>
		<p>${coinFlipper.totalNumberOfHeads} flips were Heads.	&nbsp&nbsp	${coinFlipper.totalNumberOfTails} flips were Tails.</p>
	</div>
</div>
</body>
</html>