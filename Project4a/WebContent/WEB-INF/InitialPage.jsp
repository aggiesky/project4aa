<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="css/Basic.css" rel=stylesheet type="text/css" media="screen">
<title>Coin Flipper</title>

</head>
<body>

<!-- This jsp shows 3 different views depending on the state of the app.
     There is and InitialPage, a MainPage, and an EndingPage.  Sections
     are conditionally shown/not shown as appropriate for each page.
      -->
<div class="top-level">
<h1>Welcome to the <span style="font-style: italic;">Coin Flipper</span></h1>

	<!-- Top message & results of last game section.  -->
	<div class="section">
		<c:if test='${nextPage eq "InitialPage"}'>
			<h3>Select the "Heads" or "Tails" button to play the game.</h3>
		</c:if>
		<c:if test='${nextPage eq "MainPage"}'>
			<h3>${coinFlipper.bannerForPreviousGame}</h3>
			<h3 style="font-weight: bolder;">The Guess was: ${coinFlipper.previousGuess}	&nbsp &nbsp &nbsp	The Result was: ${coinFlipper.previousFlipResult}</h3>
		</c:if>
		<c:if test='${nextPage eq "EndingPage"}'>
				<h2>Thanks for playing!</h2>
		</c:if>
	</div>
	
	
	<!-- The button section -->
	<!-- InitialPage:  Shows 2 buttons: Heads, Tails
	     MainPage:  Shows 4 buttons:  Heads, Tails, End Game, Reset
	     Ending Page: Shows 1 button: Restart (actually located further down in the page.
	      -->
	<div class="section" style="padding-bottom: 40px; margin-left: 10px;">
		<c:if test='${not (nextPage eq "EndingPage")}'>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Heads">
		        </form>
		    </div>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Tails">
		        </form>
		    </div>
	    </c:if>
	 	<c:if test='${nextPage eq "MainPage"}'>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="EndGame">
		        </form>
		    </div>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Reset">
		        </form>
		    </div>    
	    </c:if>
	</div>
	
	<hr style="width: 75%; float: left;">
	<br>
	
	
	<!-- The statistics section is shown on every page view. -->
	<div class="section">	
		<h2>Game Statistics</h2>
		<p>Games Played:  ${coinFlipper.totalNumberOfFlips}  &nbsp&nbsp  Winning percentage: ${coinFlipper.totalWinningPercentage} %</p>
		<p>${coinFlipper.totalNumberOfHeads} flips were Heads.	&nbsp&nbsp	${coinFlipper.totalNumberOfTails} flips were Tails.</p>
	</div>
	
	
	<!-- The Restart button is only shown on the EndingPage view. -->
		 <c:if test='${nextPage eq "EndingPage"}'>
		    <div class="button"> 
		        <form method="POST" action="main" >
		            <input type="submit" name="userGuess" value="Restart">
		        </form>
		    </div>
	    </c:if>
</div>

</body>
</html>